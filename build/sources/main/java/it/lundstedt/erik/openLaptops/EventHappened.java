package it.lundstedt.erik.openLaptops;

import it.lundstedt.erik.openLaptops.blocks.MyBlocks;
import it.lundstedt.erik.openLaptops.items.MyItems;
import net.minecraft.block.Block;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraft.item.Item;
@EventBusSubscriber(modid= it.lundstedt.erik.openLaptops.OpenLaptops.MODID)
public class EventHappened {
	@SubscribeEvent
	public static void itemRegisterEvent(RegistryEvent.Register<Item> event){

		for (int i = 0; i < MyItems.items.size(); i++) {
			event.getRegistry().register(MyItems.items.get(i));
		}
	}
	@SubscribeEvent
	public static void blockRegisterEvent(RegistryEvent.Register<Block> event){
		for (int i = 0; i < MyBlocks.blocks.size(); i++) {
			event.getRegistry().register(MyBlocks.blocks.get(i));
		}
	}





}
