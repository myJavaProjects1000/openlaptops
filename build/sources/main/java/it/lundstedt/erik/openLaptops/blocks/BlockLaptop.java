package it.lundstedt.erik.openLaptops.blocks;
import li.cil.oc.OpenComputers;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

import javax.annotation.Nullable;

public class BlockLaptop extends BlockContainer
{

protected BlockLaptop(Material materialIn) {
	super(materialIn);
}

protected BlockLaptop(Material materialIn, MapColor color) {
	super(materialIn, color);
}

@Nullable
@Override
public TileEntity createNewTileEntity(World worldIn, int meta) {
	return null;
}
}
