package it.lundstedt.erik.openLaptops;
import it.lundstedt.erik.openLaptops.blocks.MyBlocks;
import it.lundstedt.erik.openLaptops.items.MyItems;
import it.lundstedt.erik.openLaptops.tabs.MyTabs;
import net.minecraft.init.Blocks;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import org.apache.logging.log4j.Logger;

import java.util.function.Consumer;

@Mod(modid = it.lundstedt.erik.openLaptops.OpenLaptops.MODID, name = it.lundstedt.erik.openLaptops.OpenLaptops.NAME, version = it.lundstedt.erik.openLaptops.OpenLaptops.VERSION,dependencies = "required-after:opencomputers@[1.7.5,)")
public class OpenLaptops {
	public static final String MODID = "openlaptops";
	public static final String NAME = "OpenLaptops";
	public static final String VERSION = "0.0.0.1";
/*
	public static Thread getterThread = new Thread(new Runnable()
	{
		@Override public void run() {
	}
	public void jwpost(){
		try {
			Net.get(new ToggleSettings());
		} catch (Exception e) {e.printStackTrace();}
		getterThread.interrupt();
		}
	
	});
 */
	
	
	public static Logger logger;
	public static Consumer<String> printer = text ->{logger.info(text);};
	@EventHandler
	public void preInit(FMLPreInitializationEvent event){
		logger = event.getModLog();
		MyTabs.init();
		MyBlocks.init();
		MyItems.init();
	}

	@EventHandler
	public void init(FMLInitializationEvent event){
		// some example code
		logger.info("DIRT BLOCK >> {}", Blocks.DIRT.getRegistryName());
		logger.info("hello World");
	}
	
	

	
	
}
