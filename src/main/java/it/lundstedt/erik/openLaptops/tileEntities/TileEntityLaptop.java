package it.lundstedt.erik.openLaptops.tileEntities;


import li.cil.oc.api.driver.DriverItem;
import li.cil.oc.api.internal.Server;
import li.cil.oc.api.machine.Machine;
import li.cil.oc.api.network.Connector;
import li.cil.oc.api.network.ManagedEnvironment;
import li.cil.oc.api.network.Node;
import li.cil.oc.api.util.Lifecycle;
import li.cil.oc.common.tileentity.Robot;
import li.cil.oc.common.tileentity.traits.*;
import li.cil.oc.util.BlockPosition;
import li.cil.oc.util.StackOption;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import scala.Function1;
import scala.Function2;
import scala.Option;
import scala.collection.Iterable;
import scala.collection.mutable.ArrayBuffer;

import java.util.EnumSet;
//Computer, PowerAcceptor,
public class TileEntityLaptop extends TileEntity implements ISidedInventory, RotatableTile
{
	
	
	@Override
	public EnumFacing pitch() {
		return RotatableTile.super.pitch();
	}
	
	@Override
	public void pitch_$eq(EnumFacing value) {
		RotatableTile.super.pitch_$eq(value);
	}
	
	@Override
	public EnumFacing yaw() {
		return RotatableTile.super.yaw();
	}
	
	@Override
	public void yaw_$eq(EnumFacing value) {
		RotatableTile.super.yaw_$eq(value);
	}
	
	@Override
	public boolean setFromEntityPitchAndYaw(Entity entity) {
		return RotatableTile.super.setFromEntityPitchAndYaw(entity);
	}
	
	@Override
	public boolean setFromFacing(EnumFacing value) {
		return RotatableTile.super.setFromFacing(value);
	}
	
	@Override
	public boolean invertRotation() {
		return RotatableTile.super.invertRotation();
	}
	
	@Override
	public EnumFacing facing() {
		return RotatableTile.super.facing();
	}
	
	@Override
	public boolean rotate(EnumFacing axis) {
		return RotatableTile.super.rotate(axis);
	}
	
	@Override
	public EnumFacing toLocal(EnumFacing value) {
		return RotatableTile.super.toLocal(value);
	}
	
	@Override
	public EnumFacing toGlobal(EnumFacing value) {
		return RotatableTile.super.toGlobal(value);
	}
	
	@Override
	public EnumFacing[] validFacings() {
		return RotatableTile.super.validFacings();
	}
	
	@Override
	public void onRotationChanged() {
		RotatableTile.super.onRotationChanged();
	}
	
	@Override
	public void updateTranslation() {
		RotatableTile.super.updateTranslation();
	}
	
	@Override
	public int x() {
		return RotatableTile.super.x();
	}
	
	@Override
	public int y() {
		return RotatableTile.super.y();
	}
	
	@Override
	public int z() {
		return RotatableTile.super.z();
	}
	
	@Override
	public BlockPosition position() {
		return RotatableTile.super.position();
	}
	
	@Override
	public boolean isClient() {
		return RotatableTile.super.isClient();
	}
	
	@Override
	public boolean isServer() {
		return RotatableTile.super.isServer();
	}
	
	@Override
	public void updateEntity() {
		RotatableTile.super.updateEntity();
	}
	
	@Override
	public void initialize() {
		RotatableTile.super.initialize();
	}
	
	@Override
	public void dispose() {
		RotatableTile.super.dispose();
	}
	
	@Override
	public void readFromNBTForServer(NBTTagCompound nbt) {
		RotatableTile.super.readFromNBTForServer(nbt);
	}
	
	@Override
	public void writeToNBTForServer(NBTTagCompound nbt) {
		RotatableTile.super.writeToNBTForServer(nbt);
	}
	
	@Override
	public void readFromNBTForClient(NBTTagCompound nbt) {
		RotatableTile.super.readFromNBTForClient(nbt);
	}
	
	@Override
	public void writeToNBTForClient(NBTTagCompound nbt) {
		RotatableTile.super.writeToNBTForClient(nbt);
	}
	
	@Override
	public boolean trySetPitchYaw(EnumFacing pitch, EnumFacing yaw) {
		return RotatableTile.super.trySetPitchYaw(pitch, yaw);
	}
	
	@Override
	public int[] getSlotsForFace(EnumFacing side) {
		return new int[0];
	}
	
	@Override
	public boolean canInsertItem(int index, ItemStack itemStackIn, EnumFacing direction) {
		return false;
	}
	
	@Override
	public boolean canExtractItem(int index, ItemStack stack, EnumFacing direction) {
		return false;
	}
	
	@Override
	public int getSizeInventory() {
		return 0;
	}
	
	@Override
	public boolean isEmpty() {
		return false;
	}
	
	@Override
	public ItemStack getStackInSlot(int index) {
		return null;
	}
	
	@Override
	public ItemStack decrStackSize(int index, int count) {
		return null;
	}
	
	@Override
	public ItemStack removeStackFromSlot(int index) {
		return null;
	}
	
	@Override
	public void setInventorySlotContents(int index, ItemStack stack) {
	
	}
	
	@Override
	public int getInventoryStackLimit() {
		return 0;
	}
	
	@Override
	public boolean isUsableByPlayer(EntityPlayer player) {
		return false;
	}
	
	@Override
	public void openInventory(EntityPlayer player) {
	
	}
	
	@Override
	public void closeInventory(EntityPlayer player) {
	
	}
	
	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack) {
		return false;
	}
	
	@Override
	public int getField(int id) {
		return 0;
	}
	
	@Override
	public void setField(int id, int value) {
	
	}
	
	@Override
	public int getFieldCount() {
		return 0;
	}
	
	@Override
	public void clear() {
	
	}
	
	@Override
	public String getName() {
		return null;
	}
	
	@Override
	public boolean hasCustomName() {
		return false;
	}
}
