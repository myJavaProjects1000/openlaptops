package it.lundstedt.erik.openLaptops.blocks;


import it.lundstedt.erik.openLaptops.OpenLaptops;
import it.lundstedt.erik.openLaptops.items.MyItems;
import it.lundstedt.erik.openLaptops.tabs.MyTabs;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemBlock;

import java.util.ArrayList;
import java.util.List;


public class MyBlocks
{
	public static List<Block> blocks=new ArrayList<>();
	public static final Block console = registerBlock("console",new Block(Material.ANVIL),MyTabs.tab);
	//public static final Block console = registerBlock("console",new Block(Material.ANVIL),CreativeTabs.MISC);//bugtesting
	public static Block registerBlock(String name, Block block) {
		block.setUnlocalizedName(name);
		block.setRegistryName(it.lundstedt.erik.openLaptops.OpenLaptops.MODID,name);
		blocks.add(block);
		return block;
	}
	public static Block registerBlock(String name, Block block, CreativeTabs tab) {
		Block newBlock=registerBlock(name, block);
		newBlock.setCreativeTab(tab);
		MyItems.registerItem(name,new ItemBlock(newBlock),tab);
		return newBlock;
	}
	
	public static void init () {

	}



}
