package it.lundstedt.erik.openLaptops.utils;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.util.text.TextComponentString;
import org.apache.logging.log4j.Logger;

import java.util.function.Consumer;
import java.util.function.Supplier;

public class Utilities
{
	
	public static Logger logger;
	public static Consumer<String> printer = text ->{logger.info(text);};
	
	
	public static Consumer<String> getChatConsumer(EntityPlayer playerIn)
	{
		return new Consumer<String>() {
			@Override
			public void accept(String message ) {
				playerIn.sendStatusMessage(new TextComponentString(message),false);
			}
		};
	}
	
	public static Supplier<Item> getItemSuplier(Item item)
	{
		Supplier<Item> result=new Supplier<Item>() {@Override public Item get(){return item;}};
		return result;
	}
	
	
	
	
	
	
	
	
}
